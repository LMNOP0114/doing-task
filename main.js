let tasks = [
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'pending'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'doing'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'done'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'rejected'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'pending'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'doing'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'done'
},
{
    title: 'Motor',
    startDate: '05,05,2021',
    endDate: '09,05,2021',
    stuff: 'John',
    status: 'rejected'
},
];

let pending =  document.getElementById("pendingList");
let doing = document.getElementById ("doingList");
let done = document.getElementById("doneList");
let rejected = document.getElementById("rejectedList");
pending.innerHTML = ''


function Drawlist(){
    tasks.map( (v,i) =>{
        if (v.status === "pending"){
            pending.innerHTML += `  <h6 class="mt-2"> Task title ${v.title} </h6>
            <h6>Start date: ${v.startDate}</h6>
            <h6>End date: ${v.endDate}</h6>
            <h6>Stuff: ${v.stuff}</h6>
            <h6>Status: ${v.status}</h6>
            <select name="status" id="" class="form-control mt-2">
              <option disabled selected>Select status</option>
              <option value="doing">Doing</option>
              <option value="done">Done</option>
            </select>
            <button class="btn btn-warning mt-2">Edit</button>
            <button class="btn btn-danger mt-2">Delete</button>
            <hr class="bg-dark"></hr>`
        }
        if (v.status === "doing"){
            doing.innerHTML += `  <h6 class="mt-2"> Task title ${v.title} </h6>
            <h6>Start date: ${v.startDate}</h6>
            <h6>End date: ${v.endDate}</h6>
            <h6>Stuff: ${v.stuff}</h6>
            <h6>Status: ${v.status}</h6>
            <select name="status" id="" class="form-control mt-2">
              <option disabled selected>Select status</option>
              <option value="doing">Doing</option>
              <option value="done">Done</option>
            </select>
            <button class="btn btn-warning mt-2">Edit</button>
            <button class="btn btn-danger mt-2">Delete</button>
            <hr class="bg-dark"></hr>`
        }
        if (v.status === "done"){
            done.innerHTML += `  <h6 class="mt-2"> Task title ${v.title} </h6>
            <h6>Start date: ${v.startDate}</h6>
            <h6>End date: ${v.endDate}</h6>
            <h6>Stuff: ${v.stuff}</h6>
            <h6>Status: ${v.status}</h6>
            <button type"button" class="btn btn-danger w-100">Recejted</button
            <hr class="bg-dark"></hr>`
        }
        if (v.status === "rejected"){
            rejected.innerHTML += `  <h6 class="mt-2"> Task title ${v.title} </h6>
            <h6>Start date: ${v.startDate}</h6>
            <h6>End date: ${v.endDate}</h6>
            <h6>Stuff: ${v.stuff}</h6>
            <h6>Status: ${v.status}</h6>
            <select name="status" id="" class="form-control mt-2">
              <option disabled selected>Select status</option>
              <option value="doing">Doing</option>
              <option value="done">Done</option>
            </select>
            <button class="btn btn-warning mt-2">Edit</button>
     
            <hr class="bg-dark"></hr>`
        }

    })
}
Drawlist();



// Add task


